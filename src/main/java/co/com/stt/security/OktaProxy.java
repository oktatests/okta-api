/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.stt.security;

import com.okta.sdk.clients.AuthApiClient;
import com.okta.sdk.clients.SessionApiClient;
import com.okta.sdk.clients.UserApiClient;
import com.okta.sdk.framework.ApiClientConfiguration;
import com.okta.sdk.framework.FilterBuilder;
import com.okta.sdk.models.auth.AuthResult;
import com.okta.sdk.models.sessions.Session;
import com.okta.sdk.models.users.LoginCredentials;
import com.okta.sdk.models.users.User;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author desarrollobstt
 */
public class OktaProxy {

    private final String API_TOKEN = "00Q37bIzU5SVVq91kFNKGc_3GO3TD8ujGBdd0z0DzS";
    private final String OKTA_ORGANIZATION = "https://dev-193831.oktapreview.com";
    private final ApiClientConfiguration OKTA_SETTINGS;

    public OktaProxy() {
        this.OKTA_SETTINGS = new ApiClientConfiguration(OKTA_ORGANIZATION, API_TOKEN);
    }

    public AuthResult authenticate(String login, String password) throws IOException {
        AuthApiClient authClient = new AuthApiClient(OKTA_SETTINGS);
        AuthResult authResult = authClient.authenticate(login, password, null);
        return authResult;
    }

    void administrativeChangePassword(String login, String newPassword) throws IOException {
        UserApiClient userApiClient = new UserApiClient(OKTA_SETTINGS);
        User user = findUserByLogin(login);
        userApiClient.setPassword(user.getId(), newPassword);
    }

    public User findUserByLogin(String login) throws IOException {
        UserApiClient userApiClient = new UserApiClient(OKTA_SETTINGS);
        FilterBuilder filterBuilder = new FilterBuilder().where("profile.login").equalTo(login);
        List<User> usersWithFilter = userApiClient.getUsersWithFilter(filterBuilder);
        User user = null;
        //Validate exists and only one (Okta uniqueness also satisfies this)
        if (!usersWithFilter.isEmpty() && usersWithFilter.size() == 1) {
            user = usersWithFilter.get(0);
        }
        return user;
    }

    //TODO: why the credentials change password requires a state token?
    public LoginCredentials changePassword(String id, String oldPassword, String newPassword) throws IOException {
        UserApiClient userApiClient = new UserApiClient(OKTA_SETTINGS);
        LoginCredentials loginCredentials = userApiClient.changePassword(id, oldPassword, newPassword);
        return loginCredentials;
    }

    public AuthResult forgotPassword(String login) throws IOException {
        AuthApiClient authApiClient = new AuthApiClient(OKTA_SETTINGS);
        AuthResult authResult = authApiClient.forgotPassword(login, null);
        return authResult;
    }

    public AuthResult verifyRecoveryToken(String recoveryToken) throws IOException {
        AuthApiClient authApiClient = new AuthApiClient(OKTA_SETTINGS);
        AuthResult authResult = authApiClient.validateRecoveryToken(recoveryToken);
        return authResult;
    }

    public AuthResult answerRecoveryQuestion(String stateToken, String answer) throws IOException {
        AuthApiClient authApiClient = new AuthApiClient(OKTA_SETTINGS);
        //TODO: Using unlock as forget requires new password, which is not really neccessary and seems to generate an unexpected response
        AuthResult authResult = authApiClient.unlockAccountAnswer(stateToken, null, answer);
        return authResult;
    }

    public AuthResult resetPassword(String stateToken, String newPassword) throws IOException {
        AuthApiClient authApiClient = new AuthApiClient(OKTA_SETTINGS);
        AuthResult authResult = authApiClient.resetPassword(stateToken, null, newPassword);
        return authResult;
    }

    public Session createSession(String sessionToken) throws IOException {
        SessionApiClient sessionApiClient = new SessionApiClient(OKTA_SETTINGS);
        Session session = sessionApiClient.createSessionWithSessionTokenAndAdditionalFields(sessionToken, "cookieToken");
        return session;
    }

    public void logout(String id) throws IOException {
        SessionApiClient sessionApiClient = new SessionApiClient(OKTA_SETTINGS);
        sessionApiClient.clearSession(id);
    }

    public User createUser(User user) throws IOException {
        UserApiClient userApiClient = new UserApiClient(OKTA_SETTINGS);
        User user1 = userApiClient.createUser(user, true);
        return user1;
    }
}
