/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.stt.agendate;

import co.com.stt.security.OktaProxy;
import com.okta.sdk.models.auth.AuthResult;
import com.okta.sdk.models.sessions.Session;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author fmpaezri
 */
@Controller
public class FrontController {

    private OktaProxy oktaProxy = new OktaProxy();

    @RequestMapping(path = {"/login"}, method = {RequestMethod.POST, RequestMethod.GET})
    public String login(
            @RequestParam(value = "name",
                    defaultValue = "") String name,
            @RequestParam(value = "pass",
                    defaultValue = "") String pass,
            HttpServletResponse response) {
        try {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Name: " + name + " Pass: " + pass);
            if (name == null || pass == null || "".equals(name) || "".equals(pass)) {
                Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Empty credentials");
                return "login";
            }
            AuthResult authenticate = oktaProxy.authenticate(name, pass);
            Session session = oktaProxy.createSession(authenticate.getSessionToken());
            //TODO: use redirect method for SSO
            response.addCookie(new Cookie(SecurityFilter.APP_COOKIE, session.getCookieToken()));
        } catch (IOException ex) {
            Logger.getLogger(FrontController.class.getName()).log(Level.SEVERE, "Error during login", ex);
            return "login";
        }
        return "index";
    }

    @RequestMapping(path = {"/index", "/"})
    public String index() {
        return "index";
    }

    @RequestMapping("/logout")
    public String logout(HttpServletResponse response, HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                cookie.setMaxAge(0);
                cookie.setValue(null);
                response.addCookie(cookie);
            }
        }
        return "login";
    }
    
    @RequestMapping
    public String create(
            @RequestParam(name = "login", defaultValue = "")
            String login,
            @RequestParam(name = "email", defaultValue = "")
            String email,
            @RequestParam(name = "secondEmail", defaultValue = "")
            String secondEmail,
            @RequestParam(name = "firstName", defaultValue = "")
            String firstName,
            @RequestParam(name = "lastName", defaultValue = "")
            String lastName,
            @RequestParam(name = "password", defaultValue = "")
            String password,
            @RequestParam(name = "confirmPassword", defaultValue = "")
            String confirmPassword,
            @RequestParam(name = "recoveryQuestion", defaultValue = "")
            String recoveryQuestion,
            @RequestParam(name = "answer", defaultValue = "")
            String answer,
            Model model
        ){
        if("".equals(login) || "".equals(email) || "".equals(secondEmail) || "".equals(firstName) || "".equals(lastName) || "".equals(password) || "".equals(confirmPassword) || "".equals(recoveryQuestion) || "".equals(answer)){
            model.addAttribute("message", "All fields are required");
        }
//        if(login || email || secondEmail || firstName || lastName || password || confirmPassword || recoveryQuestion || answer ){
//            
//        }
        return "index";
    }

}
