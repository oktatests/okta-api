/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.stt.agendate;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author fmpaezri
 */
public class SecurityFilter implements Filter {

    public static final String APP_COOKIE = "sid";

    public SecurityFilter() {
    }

    @Override
    public void init(FilterConfig fc) throws ServletException {
        Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Initializing filter");
    }

    @Override
    public void doFilter(ServletRequest sr, ServletResponse sr1, FilterChain fc) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) sr;
        HttpServletResponse response = (HttpServletResponse) sr1;
        Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Request: " + request + " Response: " + response);
        Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Request URI: " + request.getRequestURI());
        if ("/login".equals(request.getRequestURI()) || "login".equals(request.getRequestURI())) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Allowing flow to login");
            fc.doFilter(request, response);
            return;
        }
        Cookie[] cookies = request.getCookies();
        boolean sessionCookieExists = false;
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (APP_COOKIE.equals(cookie.getName())) {
                    sessionCookieExists = true;
                }
            }
        }
        //TODO: validate session
        if (!sessionCookieExists) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Cookie doesn not exist, redirecting to login");
            response.sendRedirect("/login");
            return;
        }
        Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Cookie exists, allowinf flow");
        fc.doFilter(request, response);
    }

    @Override
    public void destroy() {
        Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Destroying filter");
    }
}
