/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.stt.security;

import com.okta.sdk.exceptions.ApiException;
import com.okta.sdk.models.auth.AuthResult;
import com.okta.sdk.models.sessions.Session;
import com.okta.sdk.models.users.LoginCredentials;
import com.okta.sdk.models.users.Password;
import com.okta.sdk.models.users.RecoveryQuestion;
import com.okta.sdk.models.users.User;
import com.okta.sdk.models.users.UserProfile;
import java.io.IOException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author desarrollobstt
 */
public class OktaProxyTest {

    private static final String NOT_FOUND = "E0000007";
    static final String ANSWER = "the answer to the question";
    static OktaProxy oktaProxy;
    static final String login = "pepito@mail.com";
    static final String originalPassword = "Pepe1234";
    static final String newPassword = "PePe1234";
    static final String AUTHENTICATION_FAILED = "E0000004";

    public OktaProxyTest() {
    }

    @BeforeClass
    public static void setUpClass() throws IOException {
        oktaProxy = new OktaProxy();
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() throws IOException {
        oktaProxy.administrativeChangePassword(login, originalPassword);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSuccessfulAuthentication() throws IOException {
        AuthResult authResult = oktaProxy.authenticate(login, originalPassword);
        boolean successful = (new AuthResult.Status()).SUCCESS.equals(authResult.getStatus());
        assertTrue(successful);
    }

    @Test
    public void testFailedAuthentication() throws IOException {
        try {
            oktaProxy.authenticate(login, "invalidPassword");
        } catch (ApiException apiException) {
            assertEquals(AUTHENTICATION_FAILED, apiException.getErrorResponse().getErrorCode());
        }
    }

    @Test
    public void testChangePassword() throws IOException {
        User user = oktaProxy.findUserByLogin(login);
        LoginCredentials loginCredentials = oktaProxy.changePassword(user.getId(), originalPassword, newPassword);
        //TODO: Is this a valid condition to assert?
        assertNull(loginCredentials.getPassword().getValue());
    }

    @Test
    public void testForgotPassword() throws IOException {
        AuthResult authResult = oktaProxy.forgotPassword(login);
        AuthResult authResult2 = oktaProxy.verifyRecoveryToken(authResult.getRecoveryToken());
        AuthResult authResult3 = oktaProxy.answerRecoveryQuestion(authResult2.getStateToken(), ANSWER);
        AuthResult authResult4 = oktaProxy.resetPassword(authResult3.getStateToken(), newPassword);
        boolean successful = (new AuthResult.Status()).SUCCESS.equals(authResult4.getStatus());
        assertTrue(successful);
    }

    @Test
    public void testSuccessfulSessionCreate() throws IOException {
        AuthResult authResult = oktaProxy.authenticate(login, originalPassword);
        Session session = oktaProxy.createSession(authResult.getSessionToken());
        assertNotNull(session.getCookieToken());
    }

    @Test
    public void testFailedSessionCreate() throws IOException {
        try {
            oktaProxy.createSession("invalidSessionToken");
        } catch (ApiException apiException) {
            assertEquals(AUTHENTICATION_FAILED, apiException.getErrorResponse().getErrorCode());
        }
    }

    @Test
    public void testLogout() throws IOException {
        AuthResult authResult = oktaProxy.authenticate(login, originalPassword);
        Session session = oktaProxy.createSession(authResult.getSessionToken());
        try {
            oktaProxy.logout(session.getId());
        } catch (ApiException apiException) {
            assertEquals(NOT_FOUND, apiException.getErrorResponse().getErrorCode());
        }
    }

    //@Test
    public void testSuccessfulUserCreation() throws IOException {       
        UserProfile userProfile = new UserProfile();
        userProfile.setLogin("newuser@mail.com");
        userProfile.setFirstName("New");
        userProfile.setLastName("User");
        userProfile.setEmail("newUser@mail.com");
        userProfile.setSecondEmail("fpaez@stt.com.co");
        
        Password password = new Password();
        password.setValue("usrTest12345");
        
        RecoveryQuestion recoveryQuestion = new RecoveryQuestion();
        recoveryQuestion.setQuestion("What is your name?");
        recoveryQuestion.setAnswer(userProfile.getFirstName() + " " + userProfile.getLastName());
        
        LoginCredentials loginCredentials = new LoginCredentials();
        loginCredentials.setPassword(password);
        loginCredentials.setRecoveryQuestion(recoveryQuestion);
        
        User user = new User();
        user.setProfile(userProfile);
        user.setCredentials(loginCredentials);
        
        User createUser = oktaProxy.createUser(user);
        assertNotNull(createUser.getId());
        assertEquals("ACTIVE", createUser.getStatus());
    }
}
